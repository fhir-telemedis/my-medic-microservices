package pl.telemedis.services.opentok;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import pl.telemedis.services.opentok.interceptor.OAuth2Interceptor;

@Configuration
public class OpenTokMvcConfig extends WebMvcConfigurerAdapter {
	
	@Autowired
	private OAuth2Interceptor oAuth2Interceptor;
	
	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(oAuth2Interceptor);
	}

}
