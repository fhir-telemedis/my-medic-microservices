package pl.telemedis.services.opentok.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

@ControllerAdvice
public class RestExceptionHandler {

    @ExceptionHandler(ResourceNotExistsException.class)
    protected ResponseEntity<Object> handleNotExists(ResourceNotExistsException ex, WebRequest request) {
        return new ResponseEntity<Object>(new ErrorMessage(ex.getMsg()), HttpStatus.NOT_FOUND);
    }
    
    @ExceptionHandler(AuthorizationException.class)
    protected ResponseEntity<Object> handleNotAuthorization(AuthorizationException ex, WebRequest request) {
        return new ResponseEntity<Object>(new ErrorMessage(ex.getMsg()), HttpStatus.UNAUTHORIZED);
    }
    
}
