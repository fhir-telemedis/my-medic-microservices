package pl.telemedis.services.opentok.exception;

public class ResourceNotExistsException extends Exception {

	private static final long serialVersionUID = 1L;
	private String msg;

	public ResourceNotExistsException(String msg) {
		this.msg = msg;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

}
