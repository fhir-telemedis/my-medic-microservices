package pl.telemedis.services.opentok.service;


import com.opentok.OpenTok;
import com.opentok.exception.OpenTokException;

import pl.telemedis.services.opentok.controller.OpenTokController;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class OpenTokService {
	private static final Logger LOGGER = LoggerFactory.getLogger(OpenTokController.class);

	public String getSession() throws OpenTokException {
		
					
		// inside a class or method...
		int apiKey = 46139882; // YOUR API KEY
		String apiSecret = "4713076726f3f7b3f4cd47bb0baa7f93764666d2";
		String sessionId=null;
		
		try {
				OpenTok opentok = new OpenTok(apiKey, apiSecret);		
				LOGGER.info("Return opentok={}", opentok);
			
				sessionId = opentok.createSession().getSessionId();
				LOGGER.info("Return sessionId={}", sessionId);
				
		    } catch (OpenTokException e) {
	          e.printStackTrace();
	     }
		
		return sessionId;
	}
	
	public String getToken(String sessionId) throws OpenTokException {
		
		// inside a class or method...
		int apiKey = 46139882; // YOUR API KEY
		String apiSecret = "4713076726f3f7b3f4cd47bb0baa7f93764666d2";
				
		OpenTok opentok = new OpenTok(apiKey, apiSecret);	
		LOGGER.info("Return opentok={}", opentok);
		
		// Generate a token from just a sessionId (fetched from a database)
		
		String token=null;
		try {
             token = opentok.generateToken(sessionId);
             LOGGER.info("Return token={}", token);
         } catch (OpenTokException e) {
              e.printStackTrace();
         }
	
	return token;
	}
	
}
