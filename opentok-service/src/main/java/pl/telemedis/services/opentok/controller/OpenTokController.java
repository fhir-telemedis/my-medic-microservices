package pl.telemedis.services.opentok.controller;

import org.hl7.fhir.dstu3.model.Bundle;
import org.hl7.fhir.dstu3.model.Person;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;

import ca.uhn.fhir.rest.client.api.IGenericClient;
import ca.uhn.fhir.rest.server.exceptions.ResourceNotFoundException;
import pl.telemedis.services.opentok.service.OpenTokService;
import com.opentok.exception.OpenTokException;

@RestController
@RequestMapping("/opentok")
public class OpenTokController {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(OpenTokController.class);

	@Autowired
	IGenericClient fhirClient;
	@Autowired
	RestTemplate template;
	@Autowired
	OpenTokService openTokService;
		
	@PostMapping("/session/{id}")
	String sessionId(@PathVariable("id") String id) throws JsonProcessingException, OpenTokException {
		
		LOGGER.info("sessionForUser: id={}", id);
		Bundle bundle = fhirClient.search().forResource(Person.class).where(Person.LINK.hasId(id)).returnBundle(Bundle.class).execute();
		if (bundle.getIdElement() == null)
			throw new ResourceNotFoundException("Patient not found: id=" + id);
		String idx = bundle.getEntry().get(0).getResource().getIdElement().getIdPart();
		LOGGER.info("sessionForUser: idx={}", idx);
		String  personId = String.valueOf(bundle.getIdElement().getIdPart());
		LOGGER.info("sessionForUser: personId={}", personId);
		
		return  openTokService.getSession();
	}
	
		@PostMapping("/token/{id}")
		String token(@PathVariable("id") String sessionId) throws JsonProcessingException, OpenTokException {
		
		LOGGER.info("sessionId: id={}", sessionId);
			
		return openTokService.getToken(sessionId);
	}
}
