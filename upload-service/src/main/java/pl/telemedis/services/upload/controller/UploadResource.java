package pl.telemedis.services.upload.controller;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.hl7.fhir.dstu3.model.Binary;
import org.hl7.fhir.dstu3.model.IdType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import ca.uhn.fhir.rest.api.MethodOutcome;
import ca.uhn.fhir.rest.api.PreferReturnEnum;
import ca.uhn.fhir.rest.client.api.IGenericClient;
import pl.telemedis.services.upload.model.UploadStatus;

@RestController
public class UploadResource {

	private Logger logger = LoggerFactory.getLogger(UploadResource.class);

	@Value("${upload.path}")
	private String uploadPath;

	@Autowired
	private IGenericClient fhirClient;
	
	@PostMapping
	public UploadStatus uploadFile(@RequestParam("file") MultipartFile file) {
		logger.info("UploadResource.uploadFile: name={}, type={}, size={}", file.getOriginalFilename(), file.getContentType(), file.getSize());
		OutputStream out = null;
		MethodOutcome result = null;
		try {
			Binary binary = new Binary();
			byte[] fileBytes = IOUtils.toByteArray(file.getInputStream());
			binary.setContent(fileBytes);
			result = fhirClient.create().resource(binary).prefer(PreferReturnEnum.REPRESENTATION).execute();
			logger.info("UploadResource.uploadFile: Binary [id={}, type={}]", binary.getId(), binary.getContentType());
			File f = new File(uploadPath + result.getId().getIdPart());
			out = new FileOutputStream(f);
			out.write(fileBytes);
			logger.info("UploadResource.uploadFile: file={}", f.getPath());
		} catch (IOException e) {
			logger.error("Error uploading file", e);
		} finally {
			try {
				if (out != null) {
					out.flush();
					out.close();
				}
			} catch (IOException e) {
				logger.error("Error flushing/closing out file", e);
			}

		}
		return new UploadStatus(result.getId().getIdPart(), file.getSize(), file.getContentType());
	}

	@GetMapping(value = "/{id}", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public void getStream(@PathVariable("id") Integer id, HttpServletResponse response) throws IOException {
		logger.info("UploadResource.getStream: id={}", id);
		java.nio.file.Path path = Paths.get(uploadPath + id);
		byte[] data = Files.readAllBytes(path);
		response.getOutputStream().write(data);
	}
	
	@DeleteMapping("/{id}")
	public void deleteStream(@PathVariable("id") Integer id) {
		logger.info("UploadResource.deleteStream: id={}", id);
		File f = new File(uploadPath + id);
		if (f.exists()) {
			if (f.delete()) {
				fhirClient.delete().resourceById(new IdType("Binary", String.valueOf(id.intValue()))).execute();
			}
		}
	}
	
}
