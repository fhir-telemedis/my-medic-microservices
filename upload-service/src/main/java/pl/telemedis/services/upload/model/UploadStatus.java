package pl.telemedis.services.upload.model;

public class UploadStatus {

	private String id;
	private long size;
	private String contentType;

	public UploadStatus() {

	}

	public UploadStatus(String id, long size, String contentType) {
		this.id = id;
		this.size = size;
		this.contentType = contentType;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public long getSize() {
		return size;
	}

	public void setSize(long size) {
		this.size = size;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

}
