## Upload Service - Instrukcja

#### Instalacja

Project budujemy poleceniem:
```
mvn clean install
```

Wersja ustawiana jest na podstawie pom.xml.

`<version>{numer_wersji}</version>`

Nastepnie kopiujemy zbudowany jar upload-service-{version}.jar na serwer.
Uruchamiamy poleceniem:
```
java -jar upload-service-{version}.jar --spring.config.location=<properties_file_location>
```

Plik konfiguracyjny application.yml:

```
server:  
  port: ${port:8084}

spring:  
  application:
    name: upload-service

logging:
  file: upload.log
  path: logs/
  
security:
  level: ${SECURITY_LEVEL:none}
  apiKey: telemedis123
  expiresIn: 3600000
  
fhir:
  client:
    url: http://localhost:8082/
    
upload:
  path: /root/mymedic/upload/
```

  
#### API

**POST /upload** - upload pliku na serwer. Content-Type: multipard/form-data

Response:
{
	"id":string, // id pliku w tabeli Binary w MyMedic server
	"size":int, // rozmiar uploadowanego pliku
	"contentType":string // typ pliku
}

**GET /upload/{id}** - pobranie pliku. id - id pliku w tabeli Binary w MyMedic server

Response: byte (application/octet-stream)

**DELETE /upload/{id}** - usunięcie pliku z dysku i bazy mymedic. id - id pliku w tabeli Binary w MyMedic