package pl.telemedis.service.push.user.message;

public class TokenValidationStatus {

	private boolean valid;
	private String message;
	private Integer userId;

	public TokenValidationStatus() {

	}

	public TokenValidationStatus(boolean valid, String message, Integer userId) {
		this.valid = valid;
		this.message = message;
		this.userId = userId;
	}

	public boolean isValid() {
		return valid;
	}

	public void setValid(boolean valid) {
		this.valid = valid;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	@Override
	public String toString() {
		return "TokenValidationStatus [valid=" + valid + ", message=" + message + ", userId=" + userId + "]";
	}

}
