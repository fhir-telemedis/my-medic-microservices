package pl.telemedis.service.push.user.message;

import java.util.HashSet;
import java.util.Set;

public class User {

	private Integer id;
	private String username;
	private String name;
	private String accessToken;
	private int expiresIn;
	private Set<String> roles = new HashSet<>();
	private Integer personId;
	private Integer patientId;
	private Integer practitionerId;
	private Boolean active;
	private UserType type;
	private boolean isNew;
	private String cometchatUserId;
	private String notificationToken;

	public User() {

	}

	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	public int getExpiresIn() {
		return expiresIn;
	}

	public void setExpiresIn(int expiresIn) {
		this.expiresIn = expiresIn;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getPersonId() {
		return personId;
	}

	public void setPersonId(Integer personId) {
		this.personId = personId;
	}

	public Boolean isActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public UserType getType() {
		return type;
	}

	public void setType(UserType type) {
		this.type = type;
	}

	public Set<String> getRoles() {
		return roles;
	}

	public void setRoles(Set<String> roles) {
		this.roles = roles;
	}

	public void addRole(String role) {
		this.roles.add(role);
	}

	public Integer getPatientId() {
		return patientId;
	}

	public void setPatientId(Integer patientId) {
		this.patientId = patientId;
	}

	public Integer getPractitionerId() {
		return practitionerId;
	}

	public void setPractitionerId(Integer practitionerId) {
		this.practitionerId = practitionerId;
	}

	public boolean isNew() {
		return isNew;
	}

	public void setNew(boolean isNew) {
		this.isNew = isNew;
	}

	public String getCometchatUserId() {
		return cometchatUserId;
	}

	public void setCometchatUserId(String cometchatUserId) {
		this.cometchatUserId = cometchatUserId;
	}

	public String getNotificationToken() {
		return notificationToken;
	}

	public void setNotificationToken(String notificationToken) {
		this.notificationToken = notificationToken;
	}

	@Override
	public String toString() {
		return "UserDTO [id=" + id + ", username=" + username + ", name=" + name + ", active=" + active + ", type="
				+ type + ", isNew=" + isNew + "]";
	}

}
