package pl.telemedis.service.push.user.message;

public enum UserType {

	PATIENT, PRACTITIONER;
	
}
