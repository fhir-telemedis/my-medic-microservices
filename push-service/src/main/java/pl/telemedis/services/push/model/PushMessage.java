package pl.telemedis.services.push.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PushMessage {

	private List<String> registration_ids = new ArrayList<>();
	private Map<String, Object> data = new HashMap<>();

	public PushMessage() {
		
	}
	
	public PushMessage(String registration_id, Map<String, Object> data) {
		this.registration_ids.add(registration_id);
		this.data = data;
	}

	public List<String> getRegistration_ids() {
		return registration_ids;
	}

	public void setRegistration_ids(List<String> registration_ids) {
		this.registration_ids = registration_ids;
	}

	public Map<String, Object> getData() {
		return data;
	}

	public void setData(Map<String, Object> data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return "PushMessage [registration_ids=" + registration_ids + ", data=" + data + "]";
	}

	
}
