package pl.telemedis.services.push.controller;

import java.util.List;
import java.util.Optional;

import org.hl7.fhir.dstu3.model.Bundle;
import org.hl7.fhir.dstu3.model.Coding;
import org.hl7.fhir.dstu3.model.Identifier;
import org.hl7.fhir.dstu3.model.Person;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;

import ca.uhn.fhir.rest.client.api.IGenericClient;
import ca.uhn.fhir.rest.server.exceptions.ResourceNotFoundException;
import pl.telemedis.services.push.service.PushService;

@RestController
@RequestMapping("/push")
public class PushController {

	private static final Logger LOGGER = LoggerFactory.getLogger(PushController.class);
	private static final String PUSH_SYSTEM = "https://onesignal.com/";
	
	@Value("${auth.url}")
	private String authUrl;
	
	@Autowired
	PushService pushService;
	
	@Autowired
	RestTemplate template;
	@Autowired
	IGenericClient fhirClient;
	
	String api_id = "\"app_id\": \"6448f967-99a9-4736-952d-bd8c4841dcd2\",\n";
	String contents = "\"contents\": {\"en\": \"English Message\"},\n";
	String player_idsNo = "\"include_player_ids\":[\"";
	String player_idsNz = "\"],\n";
	String No = "\n{\n";
	String Nz = "\n}\n";
	
	
	@PostMapping("/patient/{id}")
	public String sendToPatient(@PathVariable("id") String id, @RequestBody String json) throws JsonProcessingException {
		LOGGER.info("sendToPatient: id={}, json={}, authHeader={}", id, json);
		Bundle bundle = fhirClient.search().forResource(Person.class).where(Person.LINK.hasId(id)).returnBundle(Bundle.class).execute();
		if (bundle.getIdElement() == null)
			throw new ResourceNotFoundException("Patient not found: id=" + id);
		String idx = bundle.getEntry().get(0).getResource().getIdElement().getIdPart();
		LOGGER.info("sendToPatient: id={}", idx);
		return createAndSendPushMessageWeb(idx, json);
	}
	
	@PostMapping("/practitioner/{id}")
	public String sendToPractitioner(@PathVariable("id") String id, @RequestBody String json) throws JsonProcessingException {
		LOGGER.info("sendToPractitioner: id={}, json={}", id, json);
		Bundle bundle = fhirClient.search().forResource(Person.class).where(Person.LINK.hasId(id)).returnBundle(Bundle.class).execute();
		if (bundle.getEntry().isEmpty())
			throw new ResourceNotFoundException("Practitioner not found: id=" + id);
		String idx = bundle.getEntry().get(0).getResource().getIdElement().getIdPart();
		LOGGER.info("sendToPractitioner: idx={}", idx);
		return createAndSendPushMessageWeb(idx, json);
	}
		
    @SuppressWarnings({ })
	private String createAndSendPushMessageWeb(String personId, String jsonBody) throws JsonProcessingException {
		
    	String strJsonBody = null;
    	
    	strJsonBody = new StringBuilder(api_id).append(contents).toString();	
		LOGGER.info("createAndSendPushMessageWeb1={}", strJsonBody);
		
		strJsonBody = new StringBuilder(strJsonBody).append(player_idsNo).toString();		
		LOGGER.info("createAndSendPushMessageWeb3={}", strJsonBody);
		
		Person person = fhirClient.read().resource(Person.class).withId(personId).execute();
		String token=retrieveToken(person.getIdentifier());
		LOGGER.info("token={}", token);
			
		strJsonBody = new StringBuilder(strJsonBody).append(token).toString();		
		LOGGER.info("createAndSendPushMessageWeb4={}", strJsonBody);
				
		strJsonBody = new StringBuilder(strJsonBody).append(player_idsNz).toString();
		LOGGER.info("createAndSendPushMessageWeb5={}", strJsonBody);
	
		strJsonBody = new StringBuilder(strJsonBody).append(jsonBody).toString();		
		LOGGER.info("createAndSendPushMessageWeb6={}", strJsonBody);
		
		strJsonBody = new StringBuilder(No).append(strJsonBody).toString();		
		LOGGER.info("createAndSendPushMessageWeb7={}", strJsonBody);
		
		strJsonBody = new StringBuilder(strJsonBody).append(Nz).toString();		
		LOGGER.info("createAndSendPushMessageWeb8={}", strJsonBody);
		
		return pushService.sendWebPush(strJsonBody);
	}
    
	private String retrieveToken(List<Identifier> identifiers) {
		for (Identifier identifier : identifiers) {
			if (identifier.getType() != null) {				
				Optional<Coding> coding = identifier.getType().getCoding().stream().filter(c -> c.getSystem().equals(PUSH_SYSTEM)).findAny();								
				if (coding.isPresent()) {
					LOGGER.info("coding={}", coding);
					return identifier.getValue();
				}
			}
		}
		return null;
	}

}
