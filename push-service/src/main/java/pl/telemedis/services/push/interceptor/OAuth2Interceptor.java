package pl.telemedis.services.push.interceptor;

import java.io.IOException;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hl7.fhir.dstu3.model.Coding;
import org.hl7.fhir.dstu3.model.Identifier;
import org.hl7.fhir.dstu3.model.Person;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import ca.uhn.fhir.rest.client.api.IGenericClient;
import pl.telemedis.services.push.model.UserInfo;

@Component
public class OAuth2Interceptor implements HandlerInterceptor {

	private static final Logger LOGGER = LoggerFactory.getLogger(OAuth2Interceptor.class);
	private static final String IDP_SYSTEM = "https://keycloak.com/";
	
	@Autowired
	RestTemplate template;
	@Value("${auth.url}")
	private String validateUrl;
	@Autowired
	IGenericClient fhirClient;
	
	@Override
	public boolean preHandle(HttpServletRequest req, HttpServletResponse res, Object handler) throws Exception {
		String authHeader = req.getHeader("Authorization");
		String userIdHeader = req.getHeader("UserId");
		LOGGER.info("authHeader={}", authHeader);
		LOGGER.info("userIdHeader={}", userIdHeader);
		UserInfo userInfo = null;
		if (authHeader == null || !authHeader.startsWith("Bearer ") || userIdHeader == null) {
			LOGGER.info("Header info: authHeader={}, authBearer={}, userIdHeader", authHeader,!authHeader.startsWith("Bearer "), userIdHeader);
			sendResponse(res);
			return false;
		} else {
			LOGGER.info("UserIdHeader={}", userIdHeader);
			Person p = fhirClient.read().resource(Person.class).withId(userIdHeader).execute();
			
			Optional<Identifier> i = p.getIdentifier().stream().filter(it -> it.getSystem().equals(IDP_SYSTEM)).findAny();
			
			if (!i.isPresent()) {
				LOGGER.info("res={}", res);
				sendResponse(res);
				return false;
			}
			
			LOGGER.info("AuthHeader={}", authHeader);
			authHeader = authHeader.replaceAll("Bearer ", "");
			LOGGER.info("AuthHeader={}", authHeader);
			
			String name = null;
			name= i.get().getValue();
			LOGGER.info("Realm name={}", name);
			
			userInfo = sendToKeycloak(authHeader, i.get().getValue());
			LOGGER.info("User info: userInfo={}", userInfo);
			MDC.put("userId", String.valueOf(userInfo.getSub()));
		}
		if (userInfo.getError() != null) {
			sendResponse(res);
		}
		LOGGER.info("keycloakAuth: return={}", true);
		return true;
	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
		
	}

	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
		
	}

	private void sendResponse(HttpServletResponse res) {
		res.setStatus(401);
		res.setContentType("application/json");
		try {
			res.getWriter().append("{\"message\":\"Token verification failed.\"}");
			res.getWriter().close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public UserInfo sendToKeycloak(String token, String realm) {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
		MultiValueMap<String, String> map= new LinkedMultiValueMap<String, String>();
		map.add("access_token", token);
		HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(map, headers);
		ResponseEntity<UserInfo> response = template.postForEntity(validateUrl, request, UserInfo.class, realm);
		return response.getBody();
	}
	
}
