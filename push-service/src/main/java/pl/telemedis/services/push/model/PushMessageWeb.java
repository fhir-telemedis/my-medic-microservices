package pl.telemedis.services.push.model;

import java.util.HashMap;
import java.util.Map;

public class PushMessageWeb {

	private String to;
	private Map<String, Object> data = new HashMap<>();

	public PushMessageWeb() {

	}

	public PushMessageWeb(String to, Map<String, Object> data) {
		this.to = to;
		this.data = data;
	}

	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}

	public Map<String, Object> getData() {
		return data;
	}

	public void setData(Map<String, Object> data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return "PushMessageWeb [include_player_ids:[" + to + "], data:" + data + "]";
	}
}
