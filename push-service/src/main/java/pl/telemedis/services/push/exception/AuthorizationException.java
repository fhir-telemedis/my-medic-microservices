package pl.telemedis.services.push.exception;

public class AuthorizationException extends Exception {

	private static final long serialVersionUID = -7998539193684920460L;
	
	private String msg;

	public AuthorizationException(String msg) {
		this.msg = msg;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

}
