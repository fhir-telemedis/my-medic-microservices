package pl.telemedis.services.push.service;

import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Scanner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import com.fasterxml.jackson.core.JsonProcessingException;

import pl.telemedis.services.push.controller.PushController;

@Service
public class PushService {
	private static final Logger LOGGER = LoggerFactory.getLogger(PushController.class);
	
	@Async
	public String sendWebPush(String strJsonBody) throws JsonProcessingException {
	
		String jsonResponse=null;
		
		try {
		      
		   URL url = new URL("https://onesignal.com/api/v1/notifications");
		   HttpURLConnection con = (HttpURLConnection)url.openConnection();
		   con.setUseCaches(false);
		   con.setDoOutput(true);
		   con.setDoInput(true);

		   con.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
		   con.setRequestProperty("Authorization", "Basic NGI1NzY4ZTktMmFjYS00NzJhLWE5YzItMTM5NWE4MDA0MThh");
		   con.setRequestMethod("POST");

		//   String strJsonBody = "{"
		//                      +   "\"app_id\": \"6448f967-99a9-4736-952d-bd8c4841dcd2\","
		//                      +   "\"include_player_ids\": [\"6392d91a-b206-4b7b-a620-cd68e32c3a76\"],"
		//                      +   "\"data\": {\"foo\": \"bar\"},"
		//                      +   "\"contents\": {\"en\": \"English Message\"}"
		//                      + "}";
		   
		   LOGGER.info("endWebPush={}", strJsonBody);
		   
		   System.out.println("strJsonBody:\n" + strJsonBody);

		   byte[] sendBytes = strJsonBody.getBytes("UTF-8");
		   con.setFixedLengthStreamingMode(sendBytes.length);

		   OutputStream outputStream = con.getOutputStream();
		   outputStream.write(sendBytes);

		   int httpResponse = con.getResponseCode();
		   System.out.println("httpResponse: " + httpResponse);

		   if (  httpResponse >= HttpURLConnection.HTTP_OK
		      && httpResponse < HttpURLConnection.HTTP_BAD_REQUEST) {
		      Scanner scanner = new Scanner(con.getInputStream(), "UTF-8");
		      jsonResponse = scanner.useDelimiter("\\A").hasNext() ? scanner.next() : "";
		      scanner.close();
		   }
		   else {
		      Scanner scanner = new Scanner(con.getErrorStream(), "UTF-8");
		      jsonResponse = scanner.useDelimiter("\\A").hasNext() ? scanner.next() : "";
		      scanner.close();
		   }
		   System.out.println("jsonResponse:\n" + jsonResponse);
		   
		} catch(Throwable t) {
		   t.printStackTrace();
		};	
		return jsonResponse;
	}
}
