## Release Notes: 2018-06-25
* Adding OAuth2 Keyclock verification to push-service, report-service, upload-service
Changes in production env:
+ include property auth.url to application.yml of push-service, report-service, upload-service. Default value: http://vps.mymedic.com.pl/auth/realms/master/protocol/openid-connect/userinfo

## Release Notes: 2018-07-05
* change property auth URL. Now realm name is dynamic: https://vps.mymedic.com.pl/auth/realms/{realm}/protocol/openid-connect/userinfo