## Gateway Service - Instrukcja

#### Instalacja

Project budujemy poleceniem:
```
mvn clean install
```

Wersja ustawiana jest na podstawie pom.xml.

`<version>{numer_wersji}</version>`

Nastepnie kopiujemy zbudowany jar gateway-service-{version}.jar na serwer.
Uruchamiamy poleceniem:
```
java -jar gateway-service-{version}.jar --spring.config.location=<properties_file_location>
```

Plik konfiguracyjny application.yml:

```
server:  
  port: ${PORT:8080}

spring:  
  application:
    name: gateway-service

zuul:
  routes:
    fhir: 
      path: /fhir/**
      url: http://localhost:8082
    auth: 
      path: /auth/**
      url: http://localhost:8083
    upload:
      path: /upload/**
      url: http://localhost:8084
    upload:
      path: /report/**
      url: http://localhost:8085

ribbon:
  eureka:
    enabled: false
```