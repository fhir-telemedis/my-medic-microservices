package pl.telemedis.services.report.interceptor;

import java.io.IOException;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hl7.fhir.dstu3.model.Identifier;
import org.hl7.fhir.dstu3.model.Person;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import ca.uhn.fhir.rest.client.api.IGenericClient;
import pl.telemedis.services.report.model.UserInfo;

@Component
public class OAuth2Interceptor implements HandlerInterceptor {

	private static final Logger LOGGER = LoggerFactory.getLogger(OAuth2Interceptor.class);
	private static final String PROP_KEYCLOAK_ID = "KEYCLOAK_REALM";
	
	@Autowired
	RestTemplate template;
	@Value("${auth.url}")
	private String validateUrl;
	@Autowired
	IGenericClient fhirClient;
	
	@Override
	public boolean preHandle(HttpServletRequest req, HttpServletResponse res, Object handler) throws Exception {
		String authHeader = req.getHeader("Authorization");
		String userIdHeader = req.getHeader("UserId");
		UserInfo userInfo = null;
		if (authHeader == null || !authHeader.startsWith("Bearer ")) {
			sendResponse(res);
			return false;
		} else {
			Person p = fhirClient.read().resource(Person.class).withId(userIdHeader).execute();
			Optional<Identifier> i = p.getIdentifier().stream().filter(it -> it.getSystem().equals(PROP_KEYCLOAK_ID)).findAny();
			if (!i.isPresent()) {
				sendResponse(res);
				return false;
			}
			authHeader = authHeader.replaceAll("Bearer ", "");
			userInfo = sendToKeycloak(authHeader, i.get().getValue());
			LOGGER.info("User info: id={}, username={}", userInfo.getSub(), userInfo.getName());
			MDC.put("userId", String.valueOf(userInfo.getSub()));
		}
		if (userInfo.getError() != null) {
			sendResponse(res);
		}
		return true;
	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
		
	}

	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
		
	}

	private void sendResponse(HttpServletResponse res) {
		res.setStatus(401);
		res.setContentType("application/json");
		try {
			res.getWriter().append("{\"message\":\"Token verification failed.\"}");
			res.getWriter().close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public UserInfo sendToKeycloak(String token, String realm) {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
		MultiValueMap<String, String> map= new LinkedMultiValueMap<String, String>();
		map.add("access_token", token);
		HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(map, headers);
		ResponseEntity<UserInfo> response = template.postForEntity(validateUrl, request, UserInfo.class, realm);
		return response.getBody();
	}
	
}
