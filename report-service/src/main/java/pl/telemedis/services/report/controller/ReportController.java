package pl.telemedis.services.report.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import net.sf.jasperreports.engine.JasperReport;
import pl.telemedis.services.report.model.Consultation;
import pl.telemedis.services.report.service.ReportService;

@RestController
public class ReportController {

	private static final Logger LOGGER = LoggerFactory.getLogger(ReportController.class);
	
	@Autowired
	JasperReport consultationReport;
	@Autowired
	ReportService service;
	
	@ResponseBody
	@PostMapping(value = "/consultation")
	public ResponseEntity<InputStreamResource> generateConsultationReport(@RequestBody Consultation consultation) {
		LOGGER.info("ReportController.generateConsultationReport: consultation={}", consultation);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Map<String, Object> m = new HashMap<>();
		m.put("reportDate", sdf.format(new Date()));
		m.put("patient.name", formatNullString(consultation.getPatient().getFirstName() + " " + consultation.getPatient().getLastName()));
		m.put("patient.address", formatNullString(consultation.getPatient().getAddress()));
		if (consultation.getPatient().getBirthdate() != null)
			m.put("patient.birthdate", sdf.format(consultation.getPatient().getBirthdate()));
		else
			m.put("patient.birthdate", "-");
		m.put("patient.pesel", formatNullString(consultation.getPatient().getPesel()));
		if (consultation.getPatient().getAge() != 0)
			m.put("patient.age", String.valueOf(consultation.getPatient().getAge()));
		else
			m.put("patient.age", "-");
		m.put("patient.address", formatNullString(consultation.getPatient().getAddress()));
		m.put("practitioner.name", formatNullString(consultation.getPractitioner().getFirstName() + " " + consultation.getPractitioner().getLastName()));
		m.put("practitioner.npwz", formatNullString(consultation.getPractitioner().getNpwz()));
		m.put("practitioner.regon", formatNullString(consultation.getPractitioner().getRegon()));
		m.put("practitioner.practiceName", formatNullString(consultation.getPractitioner().getPracticeName()));
		m.put("practitioner.title", formatNullString(consultation.getPractitioner().getTitle()));
		m.put("practitioner.oil", formatNullString(consultation.getPractitioner().getOil()));
		m.put("practitioner.address", formatNullString(consultation.getPractitioner().getAddress()));
		m.put("reason", formatNullString(consultation.getReason()));
		m.put("medicalHistory", formatNullString(consultation.getMedicalHistory()));
		if (consultation.getDiagnosis() != null) {
			m.put("diagnosis", formatList(consultation.getDiagnosis().stream().map(it -> it.getName()).collect(Collectors.toList())));
			m.put("diagnosisValues", formatList(consultation.getDiagnosis().stream().map(it -> it.getValue()).collect(Collectors.toList())));
		}
//		m.put("documentation", formatList(consultation.getDocumentation()));
		m.put("medicalHistory", consultation.getMedicalHistory());
		m.put("prescriptions", formatList(consultation.getPrescriptions()));
		m.put("recommendations", formatList(consultation.getRecommendations()));
//		m.put("sickLeaves", formatList(consultation.getSickLeaves()));
//		m.put("referrals", formatList(consultation.getReferrals()));
		return service.generateReport(consultationReport, "consultation.pdf", m);
	}
	
	private String formatList(List<String> l) {
		if (l == null)
			return "-";
		StringBuilder builder = new StringBuilder();
		l.stream().forEach(it -> builder.append(it).append("\n"));
		return builder.toString();
	}
	
	private String formatNullString(String s) {
		if (s == null)
			return "-";
		else
			return s;
	}
	
}
