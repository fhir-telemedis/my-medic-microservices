package pl.telemedis.services.report.service;

import java.io.FileInputStream;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JRDesignStyle;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;

@Service
public class ReportService {

	private int index = 1;
	private static final Logger LOGGER = LoggerFactory.getLogger(ReportService.class);
	
	public ResponseEntity<InputStreamResource> generateReport(JasperReport jasperReport, String name, Map<String, Object> params) {
		FileInputStream st = null;
		try {
			String fileName = (index++) + name;
			JRDesignStyle style = new JRDesignStyle();
			style.setPdfEncoding("Cp1250");
			JasperPrint p = JasperFillManager.fillReport(jasperReport, params, new JREmptyDataSource());
			p.setDefaultStyle(style);
			JRPdfExporter exporter = new JRPdfExporter();
			SimpleOutputStreamExporterOutput c = new SimpleOutputStreamExporterOutput(fileName);
			exporter.setExporterInput(new SimpleExporterInput(p));
			exporter.setExporterOutput(c);
			exporter.exportReport();
			
			st = new FileInputStream(fileName);
			HttpHeaders responseHeaders = new HttpHeaders();
			responseHeaders.setContentType(MediaType.valueOf("application/pdf"));
			responseHeaders.setContentDispositionFormData("attachment", name);
			responseHeaders.setContentLength(st.available());
			LOGGER.info("Report has been successfully generated: {} (size={})", fileName, st.available());
		    return new ResponseEntity<InputStreamResource>(new InputStreamResource(st), responseHeaders, HttpStatus.OK);
		} catch (Exception e) {
			LOGGER.error("Error while generating report", e);
		}
		return null;
	}
}
