package pl.telemedis.services.report.model;

public class Practitioner {

	private String title;
	private String firstName;
	private String lastName;
	private String npwz;
	private String oil;
	private String practiceName;
	private String regon;
	private String address;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getNpwz() {
		return npwz;
	}

	public void setNpwz(String npwz) {
		this.npwz = npwz;
	}

	public String getPracticeName() {
		return practiceName;
	}

	public void setPracticeName(String practiceName) {
		this.practiceName = practiceName;
	}

	public String getRegon() {
		return regon;
	}

	public void setRegon(String regon) {
		this.regon = regon;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getOil() {
		return oil;
	}

	public void setOil(String oil) {
		this.oil = oil;
	}

	@Override
	public String toString() {
		return "Practitioner [title=" + title + ", firstName=" + firstName + ", lastName=" + lastName + ", npwz=" + npwz
				+ ", practiceName=" + practiceName + ", regon=" + regon + "]";
	}

}
