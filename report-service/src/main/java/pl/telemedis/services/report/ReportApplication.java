package pl.telemedis.services.report;

import java.io.File;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.rest.api.EncodingEnum;
import ca.uhn.fhir.rest.client.apache.ApacheRestfulClientFactory;
import ca.uhn.fhir.rest.client.api.IGenericClient;
import ca.uhn.fhir.rest.client.api.ServerValidationModeEnum;
import ca.uhn.fhir.rest.client.interceptor.LoggingInterceptor;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.engine.util.JRSaver;

@SpringBootApplication
public class ReportApplication {

	@Value("${fhir.client.url}")
	private String fhirUrl;
	
	public static void main(String[] args) {
		SpringApplication.run(ReportApplication.class, args);
	}
	
	@Bean
	IGenericClient fhirClient() {
		FhirContext ourCtx = FhirContext.forDstu3();
		ourCtx.setRestfulClientFactory(new ApacheRestfulClientFactory(ourCtx));
		ourCtx.getRestfulClientFactory().setServerValidationMode(ServerValidationModeEnum.NEVER);
		ourCtx.getRestfulClientFactory().setSocketTimeout(1200000);
		IGenericClient client = ourCtx.newRestfulGenericClient(fhirUrl);
		client.setEncoding(EncodingEnum.JSON);
		client.registerInterceptor(new LoggingInterceptor(true));
		return client;
	}

	@Bean
	JasperReport report() throws JRException {
		JasperReport jr = null;
		File f = new File("consultation.jasper");
		if (f.exists()) {
			jr = (JasperReport) JRLoader.loadObject(f);
		} else {
			jr = JasperCompileManager.compileReport("jasper/consultation.jrxml");
			JRSaver.saveObject(jr, "consultation.jasper");
		}
		return jr;
	}
	
}
