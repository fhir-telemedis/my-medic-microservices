package pl.telemedis.services.report.model;

import java.util.List;

public class Consultation {

	private Patient patient;
	private Practitioner practitioner;
	private String reason;
	private String medicalHistory;
	private List<String> documentation;
	private List<Diagnosis> diagnosis;
	private List<String> recommendations;
	private List<String> prescriptions;
	private List<String> referrals;
	private List<String> sickLeaves;

	public Patient getPatient() {
		return patient;
	}

	public void setPatient(Patient patient) {
		this.patient = patient;
	}

	public Practitioner getPractitioner() {
		return practitioner;
	}

	public void setPractitioner(Practitioner practitioner) {
		this.practitioner = practitioner;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getMedicalHistory() {
		return medicalHistory;
	}

	public void setMedicalHistory(String medicalHistory) {
		this.medicalHistory = medicalHistory;
	}

	public List<String> getDocumentation() {
		return documentation;
	}

	public void setDocumentation(List<String> documentation) {
		this.documentation = documentation;
	}

	public List<Diagnosis> getDiagnosis() {
		return diagnosis;
	}

	public void setDiagnosis(List<Diagnosis> diagnosis) {
		this.diagnosis = diagnosis;
	}

	public List<String> getRecommendations() {
		return recommendations;
	}

	public void setRecommendations(List<String> recommendations) {
		this.recommendations = recommendations;
	}

	public List<String> getPrescriptions() {
		return prescriptions;
	}

	public void setPrescriptions(List<String> prescriptions) {
		this.prescriptions = prescriptions;
	}

	public List<String> getReferrals() {
		return referrals;
	}

	public void setReferrals(List<String> referrals) {
		this.referrals = referrals;
	}

	public List<String> getSickLeaves() {
		return sickLeaves;
	}

	public void setSickLeaves(List<String> sickLeaves) {
		this.sickLeaves = sickLeaves;
	}

	@Override
	public String toString() {
		return "Consultation [patient=" + patient + ", practitioner=" + practitioner + ", reason=" + reason + "]";
	}

}
