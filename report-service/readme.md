## Report Service - Instrukcja

#### Instalacja

Project budujemy poleceniem:
```
mvn clean install
```

Wersja ustawiana jest na podstawie pom.xml.

`<version>{numer_wersji}</version>`

Nastepnie kopiujemy zbudowany jar report-service-{version}.jar na serwer.
Uruchamiamy poleceniem:
```
java -jar report-service-{version}.jar --spring.config.location=<properties_file_location>
```

Plik konfiguracyjny application.yml:

```
server:  
  port: ${port:8085}

spring:  
  application:
    name: report-service

logging:
  file: report.log
```

    
#### Report API (/)

Consultation {
	Patient patient,
	Practitioner practitioner,
	String reason,
	String medicalHistory,
	List<String> documentation,
	List<Diagnosis> diagnosis,
	List<String> recommendations,
	List<String> prescriptions,
	List<String> referrals,
	List<String> sickLeaves
}

Patient {
	String firstName,
	String lastName,
	String pesel,
	Date birthdate,
	int age,
	String address
}

Practitioner {
	String title,
	String firstName,
	String lastName,
	String npwz,
	String oil,
	String practiceName,
	String regon,
	String address
}

Diagnosis {
	String name,
	String value
}



**POST /consultation** - wygenerowanie raportu w postaci pliku PDF
Request: Consultation
Response: application/octet-stream

